Result from OAUTH (Google)

access_token: "ya29.eAC2Y2bFvjNtHBwAAADAex64QhcKmvLxLdxHdY9Z4u6OkQBiteLMa-AXWHabfg"
authuser: "0"
client_id: "885478038448-is0ieelv96rlokn5dmn6ltt9v8hfap5n.apps.googleusercontent.com"
code: "4/ykYA8JiFuwb8C-EFrYHRk2lReSQY.MjADV-_zR-ISPvB8fYmgkJyjyiaWkAI"
cookie_policy: "single_host_origin"
expires_at: "1410012315"
expires_in: "3600"
g_user_cookie_policy: "single_host_origin"
id_token: "eyJhbGciOiJSUzI1NiIsImtpZCI6ImFiZWU4YzY0YzJjOGI2ZTQ2ZDlmNDU3MjYyNDBiZGEwYWE4MzNmNGYifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwiaWQiOiIxMDk4MTU5MzA5OTgyMjA5MzU0NzQiLCJzdWIiOiIxMDk4MTU5MzA5OTgyMjA5MzU0NzQiLCJhenAiOiI4ODU0NzgwMzg0NDgtaXMwaWVlbHY5NnJsb2tuNWRtbjZsdHQ5djhoZmFwNW4uYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJhdF9oYXNoIjoiVHAxdkhsbmdGaTIya1Q2SkpibHI2ZyIsImNvZGVfaGFzaCI6IjBYNElCLWtLX0ZsQTNQbDVNZlhBR3ciLCJhdWQiOiI4ODU0NzgwMzg0NDgtaXMwaWVlbHY5NnJsb2tuNWRtbjZsdHQ5djhoZmFwNW4uYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJ0b2tlbl9oYXNoIjoiVHAxdkhsbmdGaTIya1Q2SkpibHI2ZyIsImNpZCI6Ijg4NTQ3ODAzODQ0OC1pczBpZWVsdjk2cmxva241ZG1uNmx0dDl2OGhmYXA1bi5hcHBzLmdvb2dsZXVzZXJjb250ZW50LmNvbSIsImNfaGFzaCI6IjBYNElCLWtLX0ZsQTNQbDVNZlhBR3ciLCJpYXQiOjE0MTAwMDg0MTUsImV4cCI6MTQxMDAxMjMxNX0.T316Wj3id328urV7yAa_CAtMKujS7-VDxJPMpowyudIyFoABADiSx3ERbZjQG4fZo3n2EO6tGqMbAPXVJdgov2l8U77ohD0SlZxtQq609LBWyUqI4PIsrQWb3e_JW9k3ujbcZvgd7cg8Ys-TzRQDxrfCZkmNYz-SfKwDhsZMNx4"
issued_at: "1410008715"
num_sessions: "1"
prompt: "none"
response_type: "code token id_token gsession"
scope: "https://www.googleapis.com/auth/userinfo.profile"
session_state: "b674db1701258f5af113585d8f02dbaf52734e95..2f6b"
state: ""
status: Object
token_type: "Bearer"


Result from OAUTH (FB):

AccessToken: "CAAKXYObuiJwBAFmWtAMolUTYPiZB9hEpUlQdXWjbL1zhkrlcUcFZAZA4lmi2tpWbIOqSKtZAMk2d2Yyyhwyhg0fjvXr5TqD2Fe3NpLHH7EtpZBmtzdpbgIF8Nd9kKcry6ZAmDpeDGH9gYQFQa9mbGzeVoNMPQ2FZCpUBTtDShQCCIbRWq3IzpBBMbGBTSvF8ngHBhy2KoUdx658lYkWAq9U"
expiresIn: 6553
signedRequest: "h3lFiwWvCoYM-veh984qTZlJPo8wmWrjXYqaflOPFDc.eyJhbGdvcml0aG0iOiJITUFDLVNIQTI1NiIsImNvZGUiOiJBUUFENDFhV0wtS0RfUHVNUk5UNlppdFNzdFNLVjNtTjBNQk9Nel9fVlAyd0Y5am1ZSmxrTUgxOUtwdmZaVVM2Wko4TjR0ZjltcVozWU1KRkVfa0xCaVNPSnBwVGwwZzhJVVB1aXVpRTNveHNUdnpMbVgzQUtHUDNSX1NDYUJ2SXc5WWRaUXZUc0tId1gzTWN3YkdPVjNPOGcweVpEWHdhRnhuZFJCSWc5T05mOWdGZ0RiMEpRQXV6RGt1SVlVb095QVU0OTc2aVdJeWpuTmFvVW1yLVJpUjJEUzc0bmdzSUs4VUg4WlBVOXBCbFoycUNxbUw5X2lQQnZoMGNaTWFhanpSNTE1QmdaQnZxSzRuSENqRG1hcl90YXhLQkJVbFROZ2owMmxxREZfMi1qTTJIb3hwQTdGNVhfeTB1MGpjMkROcThPem9oWTJJZkN3RFVhT2tTX1E2VCIsImlzc3VlZF9hdCI6MTQxMDAwOTA0NywidXNlcl9pZCI6IjkzNzc3MjExOTU3MzUxNiJ9"
userID: "937772119573516"

{
   "error": {
      "message": "Error validating access token: Session has expired on Sep 5, 2014 12:00pm. The current time is Sep 15, 2014 1:23am.",
      "type": "OAuthException",
      "code": 190,
      "error_subcode": 463
   }
}
